from django import forms


class CalcForm(forms.Form):
    first = forms.IntegerField(label="Первое число", widget=forms.TextInput(attrs={}))
    second = forms.IntegerField(label="Второе число", widget=forms.TextInput(attrs={}))


class WordForm(forms.Form):
    str = forms.CharField(label="Ввдедите строку")


class ClickerForm(forms.Form):
    count_santa = forms.IntegerField(widget=forms.TextInput(attrs={"id": "count_santa",
                                                   "style": "border:none;background:white;color:black;padding-left:1%;",
                                                                   "readonly": "on"}))
    count_elves = forms.IntegerField(widget=forms.TextInput(attrs={"id": "count_elves",
                                                   "style": "border:none;backgrond:white;color:black;padding-left:1%;",
                                                                   "readonly": "on"}))
    count_mail = forms.IntegerField(widget=forms.TextInput(attrs={"id": "count_mail",
                                                   "style": "border:none;background:white;color:black;padding-left:1%;",
                                                                  "readonly": "on"}))

