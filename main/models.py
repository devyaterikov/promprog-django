from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class CalcHistory(models.Model):
    date = models.DateTimeField()
    first = models.IntegerField()
    second = models.IntegerField()
    sum = models.IntegerField()
    author = models.ForeignKey(to=User, default=1, on_delete=models.CASCADE)


class ExpressionHistory(models.Model):
    expression = models.CharField(max_length=1000)
    result = models.IntegerField()


class WordHistory(models.Model):
    date = models.DateField()
    time = models.TimeField()
    str = models.CharField(max_length=100)
    count_words = models.IntegerField()
    count_chars = models.IntegerField()
    author = models.CharField(max_length=100)


class ClickerHistory(models.Model):
    date = models.DateField()
    cs = models.IntegerField(default=0)
    ce = models.IntegerField(default=0)
    cm = models.IntegerField(default=0)
