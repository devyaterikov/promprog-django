import datetime
import random

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render

# Create your views here.
from main.forms import CalcForm, WordForm, ClickerForm
from main.models import CalcHistory, ExpressionHistory, WordHistory, ClickerHistory


def get_menu(idx):
    menu = [
        {'link': '/', 'text': 'Главная', 'active': False},
        {'link': '/calc', 'text': 'Калькулятор', 'active': False},
        {'link': '/expr', 'text': 'Случайное выражение', 'active': False},
        {'link': '/hist', 'text': 'История выражений', 'active': False},
        {'link': '/str2words', 'text': 'Разбор слова', 'active': False},
        {'link': '/str_history', 'text': 'История разборов', 'active': False},
        {'link': '/clicker', 'text': 'Кликер', 'active': False}
    ]
    menu[idx]['active'] = True
    return menu


def get_menu_auth(req):
    if req.user.is_authenticated:
        return {'link': '/logout', 'text': 'Выйти'}
    else:
        return {'link': '/login', 'text': 'Войти'}


def index(req):
    return render(req, 'index.html', {
        'menu': get_menu(0),
        'log': get_menu_auth(req),
        'pages': 1,
        'auth': 'Max',
        'cDate': datetime.datetime.now(),
        'user': req.user
    })


@login_required
def calc(req):
    user = User.objects.get(username=req.user)
    f = CalcForm()
    a = ''
    b = ''
    sum = ''

    if req.method == 'POST':
        f = CalcForm(req.POST)
        a = f.data['first']
        b = f.data['second']
        if f.is_valid():
            sum = int(a) + int(b)
            record = CalcHistory(date=datetime.datetime.now(), first=a, second=b, sum=sum,
                                 author=user)
            record.save()
        else:
            sum = 'Введены неверные данные'

    return render(req, 'calc.html', {
        'menu': get_menu(1),
        'log': get_menu_auth(req),
        'first': a,
        'second': b,
        'sum': sum,
        'history': CalcHistory.objects.filter(author=user),
        'form': f
    })


def expression(req):
    nums = [0]
    res = 0
    while -2 < nums[0] < 1:
        nums[0] = random.randint(-9999, 9999)
    res += nums[0]
    for i in range(random.randint(1, 5)):
        sign = random.randint(-1, 0)
        num = random.randint(1, 9999)
        if sign == 0:
            res -= num
        else:
            res += num
        nums.append(sign)
        nums.append(num)

    record = ExpressionHistory(expression=nums, result=res)
    record.save()

    return render(req, 'expression.html', {
        'menu': get_menu(2),
        'log': get_menu_auth(req),
        'nums': nums,
        'result': res
    })


def history(req):
    all_data = ExpressionHistory.objects.all()
    his = []
    for rec in all_data:
        num = 0
        his.append({'expression': [], 'result': rec.result})
        p = ''
        for c in rec.expression:
            if c.isdigit():
                num *= 10
                if p == '-':
                    num -= int(c)
                else:
                    num += int(c)
            elif c == ',':
                his[len(his) - 1]['expression'].append(num)
                num = 0
            p = c
        his[len(his) - 1]['expression'].append(num)

    return render(req, 'history.html', {
        'menu': get_menu(3),
        'log': get_menu_auth(req),
        'history': his,
        'all': all_data
    })


@login_required
def words(req):
    user = req.user
    count_chars = 0
    count_words = 0
    count_digitals = 0
    words = []
    digitals = []
    if req.method == 'POST':
        f = WordForm(req.POST)
        str = f.data['str']
        prev = ''
        el_word = ""
        for i in str:
            count_chars += 1
            if i.isdigit():
                count_digitals += 1
                digitals.append(i)
            if i != ' ':
                el_word += i
            elif prev != ' ':
                count_words += 1
                words.append(el_word)
                el_word = ""
            prev = i
        if el_word != "" and prev != ' ':
            count_words += 1
            words.append(el_word)

        record = WordHistory(date=datetime.datetime.now(), time=datetime.datetime.now(),
                             str=str, count_words=count_words,
                             count_chars=count_chars, author=user)
        record.save()
    else:
        f = WordForm()

    return render(req, 'words.html', {
        'menu': get_menu(4),
        'log': get_menu_auth(req),
        'count_words': count_words,
        'count_digitals': count_digitals,
        'words': words,
        'digitals': digitals,
        'form': f
    })


@login_required
def words_history(req):
    all_data = WordHistory.objects.all()
    return render(req, 'words_history.html', {
        'menu': get_menu(5),
        'log': get_menu_auth(req),
        'history': all_data
    })


def clicker(req):
    if req.method == 'POST':
        f = ClickerForm(req.POST)
        if f.is_valid():
            s = f.data['count_santa']
            e = f.data['count_elves']
            m = f.data['count_mail']
            record = ClickerHistory(date=datetime.datetime.now(), cs=s, ce=e, cm=m)
            record.save()
        else:
            print('incorrect_data_form')
    else:
        data_history = ClickerHistory.objects.all()
        if len(data_history) == 0:
            record = ClickerHistory(date=datetime.datetime.now(), cs=0, ce=0, cm=0)
            record.save()
            data_history = ClickerHistory.objects.all()

        f = ClickerForm({
            'count_santa': data_history[len(data_history) - 1].cs,
            'count_mail': data_history[len(data_history) - 1].cm,
            'count_elves': data_history[len(data_history) - 1].ce
        })

    return render(req, 'clicker.html', {
        'menu': get_menu(6),
        'log': get_menu_auth(req),
        'form': f,
    })
